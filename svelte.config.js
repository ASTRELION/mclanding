import adapterAuto from '@sveltejs/adapter-auto';
import adapterBun from "svelte-adapter-bun";
import { vitePreprocess } from "@sveltejs/kit/vite";

const adapter = process.env.BUILD_ENV === "bun" ? adapterBun() : adapterAuto();

/** @type {import('@sveltejs/kit').Config} */
const config = {
    preprocess: vitePreprocess(),
    kit:
    {
        adapter: adapter
    }
};

export default config;
