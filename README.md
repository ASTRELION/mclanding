# MCLanding

Simple [SvelteKit](https://kit.svelte.dev/)-based landing page for my [Minecraft](https://minecraft.net) server.

**Features:**

- Live updating of server icon, description, player count, and latency (via [MinecraftStatusPinger](https://github.com/woodendoors7/MinecraftStatusPinger))
- Copy server address for easy access
- Link to [Dynmap](https://github.com/webbukkit/dynmap) embeded in the same site
- Link to source code (this)
- Link to donate

![Screenshot](./screenshot.png)

## Usage

### Docker

Copy the [`docker-compose.yml`](./docker-compose.yml) file, or use `docker run`:
```bash
docker run registry.gitlab.com/astrelion/mclanding:latest \
-p 3000:3000 \
-e PUBLIC_SERVER_ADDRESS="mc.example.com" \
-e PUBLIC_SOURCE_URL="https://gitlab.com/ASTRELION/mclanding" \
-e PUBLIC_MAP_URL="https://mcmap.example.com" \
-e PUBLIC_DONATE_URL="https://liberapay.com/ASTRELION"
```

*Edit the environment variables to point to your own Minecraft server address and Dynmap address. All four variables are required.*

## Build

### Node

*Create the `.env` file with the four variables required (defined above)*.

#### Production

1. `npm install`
2. `npm run build`
3. `node -r dotenv/config build`

#### Dev

1. `npm install`
2. `npm run dev`

### Docker

1. `docker build -t mclanding .`
2. `docker run mclanding [...]` *See Usage > Docker for full command*
