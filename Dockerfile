FROM oven/bun:1 as build

WORKDIR /app
ENV BUILD_ENV=bun
COPY src src
COPY static static
COPY package.json .
COPY bun.lockb .
COPY *.ts .
COPY *.js .

RUN bun install --force
RUN bun run build

FROM oven/bun:1-alpine as production

WORKDIR /app
ENV BUILD_ENV=bun
COPY --from=build /app/package.json .
COPY --from=build /app/bun.lockb .
COPY --from=build /app/build build

# https://github.com/oven-sh/bun/issues/5792
RUN bun install

ENV ORIGIN=http://0.0.0.0
ENV HOST=0.0.0.0
ENV NODE_ENV=production
ENV PORT=3000

EXPOSE ${PORT}

WORKDIR /app/build

CMD [ "bun", "run", "start" ]
