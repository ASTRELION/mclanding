import type { SupabaseObject, WorldMeta } from "$lib/types";
import type { PageLoadEvent } from "./$types";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export async function load({ fetch: svelteFetch }: PageLoadEvent)
{
    const response = await svelteFetch("/api/worlds/list");
    const files = await response.json() as SupabaseObject[];

    const worlds: Promise<WorldMeta[]> = Promise.all(
        files.map((f) => svelteFetch(`/api/worlds/meta/${f.name}`).then((r) => r.json()))
    );

    return {
        streamed: {
            worlds: worlds,
        }
    };
}
