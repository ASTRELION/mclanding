import { json } from "@sveltejs/kit";
import { env } from "$env/dynamic/public";
import mc from "minecraftstatuspinger";
import type { Description, LookupData } from "$lib/types";

/** How long in milliseconds `lookupData` should be valid. */
const STALE_TIME = 15000;
let lookupData: LookupData;
let lastLookupData: LookupData;

export async function GET(): Promise<Response>
{
    // retrieve last response if within STALE_TIME ms of last request
    const now = new Date().getTime();
    if (lookupData && lookupData.timestamp + STALE_TIME > now)
    {
        return json(lookupData);
    }

    try
    {
        const lookup = await mc.lookup({ host: env.PUBLIC_SERVER_ADDRESS });
        lookupData = {
            players: lookup.status?.players,
            description: normalizeDescription(lookup.status?.description.extra),
            latency: lookup.latency ?? -1,
            image: lookup.status?.favicon,
            success: true,
            timestamp: now
        };
        lastLookupData = lookupData;
    }
    catch (e)
    {
        const lookup: LookupData = {
            success: false,
            timestamp: now
        };

        // Only consider the server offline if we hit 2 failed lookups in a row,
        // if we had previously succeeded.
        if (lastLookupData && lastLookupData.success)
        {
            lookupData = {
                ...lastLookupData,
                timestamp: now
            };
        }
        else
        {
            lookupData = lookup;
        }

        lastLookupData = lookup;
    }

    return json(lookupData);
}

function normalizeDescription(description: any[]): Description[]
{
    const newDescription: Description[] = [];
    let currentColor = "white";
    for (const extra of description)
    {
        // inherit whatever the last color defined was
        currentColor = "color" in extra ? String(extra.color) : currentColor;
        if (extra.text.includes("\n"))
        {
            const split = extra.text.split("\n");
            for (const s of split)
            {
                newDescription.push({
                    text: s,
                    color: currentColor
                });
                newDescription.push({
                    text: "\n",
                    color: currentColor
                });
            }
            newDescription.pop();
        }
        else
        {
            newDescription.push({
                text: extra.text,
                color: currentColor
            });
        }
    }

    return newDescription;
}
