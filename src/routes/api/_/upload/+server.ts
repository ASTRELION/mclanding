import { json } from "@sveltejs/kit";
import tus from "tus-js-client";
import fs from "fs";
import { env } from "$env/dynamic/private";
import { Readable } from "stream";

export async function GET(): Promise<Response>
{
    if (!await Bun.file("./world.zip").exists())
    {
        return json("No file found to be uploaded.");
    }

    const file = fs.createReadStream("./world.zip");
    const readable = Readable.from(file);
    // https://supabase.com/docs/guides/storage/uploads/resumable-uploads
    await new Promise((resolve, reject) =>
    {
        const upload = new tus.Upload(readable, {
            endpoint: `${env.SUPABASE_STORAGE}/upload/resumable`,
            retryDelays: [0, 3000, 5000, 10000, 20000],
            headers: {
                "authorization": `Bearer ${env.SUPABASE_KEY}`,
                "x-upsert": "true",
            },
            // uploadSize: 7569879096,
            uploadLengthDeferred: true,
            uploadDataDuringCreation: true,
            removeFingerprintOnSuccess: true,
            metadata: {
                bucketName: "mc-site",
                objectName: "world.zip",
                contentType: "application/zip",
                cacheControl: "3600",
            },
            chunkSize: 6 * 1024 * 1024,
            onError: function (error)
            {
                console.log("Failed because: " + error);
                reject(error);
            },
            onProgress: function (bytesUploaded, bytesTotal)
            {
                const percentage = ((bytesUploaded / bytesTotal) * 100).toFixed(2);
                console.log(bytesUploaded, bytesTotal, percentage + "%");
            },
            onSuccess: function ()
            {
                console.log("DONE");
                resolve(true);
            },
        });

        // Check if there are any previous uploads to continue.
        upload.findPreviousUploads().then(function (previousUploads)
        {
            // Found previous uploads so we select the first one.
            if (previousUploads.length)
                {
                upload.resumeFromPreviousUpload(previousUploads[0]);
            }

            // Start the upload
            upload.start();
        });
    });

    return json("Success!");
}
