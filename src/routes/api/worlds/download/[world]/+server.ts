import { error } from "@sveltejs/kit";
import type { RequestEvent } from "./$types";
import { env } from "$env/dynamic/private";

export async function GET({ params }: RequestEvent): Promise<Response>
{
    const world = params.world;
    const response = await fetch(`${env.SUPABASE_STORAGE}/object/mc-site/${world}/world.zip`, {
        headers: {
            "authorization": `Bearer ${env.SUPABASE_KEY}`,
        },
    });

    if (!response.ok)
    {
        throw error(response.status, response.statusText);
    }

    return new Response(response.body, {
        headers: {
            "Content-Disposition": "attachment; filename=\"world.zip\"",
        },
    });
}
