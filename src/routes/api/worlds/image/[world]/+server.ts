import { env } from "$env/dynamic/private";
import type { RequestEvent } from "@sveltejs/kit";

export async function GET({ params }: RequestEvent): Promise<Response>
{
    const world = params.world;
    const response = await fetch(`${env.SUPABASE_STORAGE}/object/mc-site/${world}/server-icon.png`, {
        headers: {
            "authorization": `Bearer ${env.SUPABASE_KEY}`,
        },
    });
    const image = await response.arrayBuffer();
    return new Response(image);
}
