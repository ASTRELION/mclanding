import { env } from "$env/dynamic/private";
import type { SupabaseObject } from "$lib/types";
import { json } from "@sveltejs/kit";

export async function GET(): Promise<Response>
{
    const response = await fetch(`${env.SUPABASE_STORAGE}/object/list/mc-site`, {
        method: "POST",
        headers: {
            "authorization": `Bearer ${env.SUPABASE_KEY}`,
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "prefix": ""
        })
    });
    let worlds = await response.json() as SupabaseObject[];
    worlds = worlds.filter((x) => x.id === null); // only folders
    return json(worlds.sort((a, b) => b.name.localeCompare(a.name)));
}
