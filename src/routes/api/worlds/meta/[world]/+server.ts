import { env } from "$env/dynamic/private";
import type { SupabaseObject, WorldMeta } from "$lib/types";
import { json, type RequestEvent } from "@sveltejs/kit";

export async function GET({ params }: RequestEvent): Promise<Response>
{
    const world = params.world;
    const response = await fetch(`${env.SUPABASE_STORAGE}/object/mc-site/${world}/world.json`, {
        headers: {
            "authorization": `Bearer ${env.SUPABASE_KEY}`,
        },
    });

    const meta = await response.json() as WorldMeta;

    if (meta.fileSize === undefined)
    {
        const response = await fetch(`${env.SUPABASE_STORAGE}/object/list/mc-site`, {
            method: "POST",
            headers: {
                "authorization": `Bearer ${env.SUPABASE_KEY}`,
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "prefix": `${world}`
            })
        });

        const fileMeta = await response.json() as SupabaseObject[];
        const obj = fileMeta.find((x) => x.name === "world.zip");
        if (obj)
        {
            meta.fileSize = obj.metadata?.size;
        }
    }

    return json(meta);
}
