export type SupabaseObject =
{
    name: string,
    id?: string,
    created_at?: string,
    updated_at?: string,
    last_accessed_at?: string,
    metadata?: {
        eTag?: string,
        size?: number,
        mimetype?: string,
        cacheControl?: string,
        lastModified?: string,
        contentLength?: number,
        httpStatusCode?: number,
    },
}

export type Description =
{
    color: string,
    text: string
}

export type LookupData =
{
    players?: {
        online: number,
        max: number,
    },
    description?: Description[],
    latency?: number,
    image?: string,
    success: boolean,
    timestamp: number
}

// Run `bun run genjson` after changing this type
export type WorldMeta =
{
    /** Pretty title for the world */
    title: string,
    /** Level name as found in server.properties, e.g. "world" */
    levelName: string,
    /** Difficulty as found in server.properties, e.g. "hard" */
    difficulty: "easy" | "normal" | "hard" | "hardcore",
    /** Seed as found in server.properties, e.g. "1234" */
    seed: string,
    /** Version of the world, e.g. "1.20.4" */
    version: string,
    /** Server type, e.g. "vanilla" */
    type: "vanilla" | "fabric" | "forge",
    /** Location of the world, e.g. "1-20" */
    folder: string,
    /** Date the world was created, e.g. "2023-10-05T22:09:00Z" */
    created: Date,
    /** Date the world was last played, e.g. "2023-10-05T22:09:00Z" */
    lastPlayed: Date,
    /** World .zip file size in bytes. If nothing is provided, it will be calculated. */
    fileSize?: number,
}
