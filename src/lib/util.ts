import * as toml from "toml";

export function convertColor(color: string): string
{
    const cssColor = `--mc-${color.replaceAll("_", "-")}`;
    return cssColor;
}

export function clamp(value: number, min: number, max: number): number
{
    return Math.max(Math.min(value, max), min);
}

/**
 * Convert bytes to a human readable string. E.g. 2024 -> 2 KB.
 * @param byteCount number of bytes
 * @returns Human readable size
 */
export function bytesToHuman(byteCount: number): string
{
    const units = ["B", "KB", "MB", "GB", "TB"];
    if (byteCount === 0)
    {
        return "0 B";
    }

    const i = Math.floor(Math.log(byteCount) / Math.log(1024));
    return `${(byteCount / Math.pow(1024, i)).toFixed(2)} ${units[i]}`;
}


// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function loadSplash(): Promise<any>
{
    try
    {
        const response = await fetch("/splashes.toml");
        const content = await response.text();
        return toml.parse(content);
    }
    catch
    {
        return {};
    }
}

export async function chooseSplash(online: boolean, current = ""): Promise<string>
{
    const splashes = await loadSplash();
    if (!splashes)
    {
        return "";
    }

    const options = Array.from(new Set([...splashes.common, ...splashes[online ? "online" : "offline"]]));
    let o = options[Math.floor(Math.random() * options.length)];
    if (options.length > 1)
    {
        while (o === current)
        {
            o = options[Math.floor(Math.random() * options.length)];
        }
    }
    return o;
}

export function toTitleCase(str: string)
{
    const strings: string[] = [];
    for (const s of str.split(" "))
    {
        strings.push(s.substring(0, 1).toUpperCase() + s.substring(1));
    }
    return strings.join(" ");
}
