/** @type { import("eslint").Linter.Config } */
module.exports = {
    root: true,
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:svelte/recommended"
    ],
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint"],
    parserOptions: {
        sourceType: "module",
        ecmaVersion: 2020,
        extraFileExtensions: [".svelte"],
    },
    env: {
        browser: true,
        es2017: true,
        node: true
    },
    overrides: [
        {
            files: ["*.svelte"],
            parser: "svelte-eslint-parser",
            parserOptions: {
                parser: "@typescript-eslint/parser"
            }
        }
    ],
    rules: {
        "brace-style": ["error", "allman", { "allowSingleLine": true }],
        "semi": ["error", "always"],
        "quotes": ["warn", "double"],
        "@typescript-eslint/no-unused-vars": ["warn"],
        "@typescript-eslint/no-explicit-any": ["warn"],
        "no-tabs": ["warn"],
        "no-trailing-spaces": ["warn"],
        "space-before-blocks": ["warn", "always"],
        "consistent-return": ["warn"],
        "@typescript-eslint/explicit-function-return-type": ["warn", { allowExpressions: true }],
    }
};
